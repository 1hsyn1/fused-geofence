package com.huseyinbulbul.gpsfused;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

public class GeofenceService extends IntentService {

    public GeofenceService() {
        super("geofencing service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedManager.init(this);

        GeofencingEvent event = GeofencingEvent.fromIntent(intent);

        if (event != null && event.hasError()) {
            SharedManager.updateGeofenceEvent("event error");
        }else {
            if (event.getGeofenceTransition() == Geofence.GEOFENCE_TRANSITION_ENTER) {
                if (event.getTriggeringGeofences().get(0).getRequestId() != null){
                    SharedManager.updateGeofenceEvent(event.getTriggeringGeofences().get(0).getRequestId()+" enter event");
                }else {
                    SharedManager.updateGeofenceEvent(" enter event");
                }
            }else {
                if (event.getTriggeringGeofences().get(0).getRequestId() != null){
                    SharedManager.updateGeofenceEvent(event.getTriggeringGeofences().get(0).getRequestId()+" exit event");
                }else {
                    SharedManager.updateGeofenceEvent(" exit event");
                }
            }
        }
    }
}
