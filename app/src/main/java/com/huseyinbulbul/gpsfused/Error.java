package com.huseyinbulbul.gpsfused;

public class Error {
    public String time;
    public String error;

    public Error(String time, String error) {
        this.time = time;
        this.error = error;
    }
}
