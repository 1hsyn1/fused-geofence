package com.huseyinbulbul.gpsfused;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

public class AlarmReciever extends BroadcastReceiver {
    GoogleApiClient googleApiClient;
    @Override
    public void onReceive(final Context context, Intent intent) {
        if(!GpsUtil.hasGpsPermissions(context) || !GpsUtil.isGpsEnabled(context))
            return;

        GeoLocationProvider geoLocationProvider = new GeoLocationProvider(context);
        geoLocationProvider.connect(); // FIXME do we have to disconnect at the end?
        geoLocationProvider.setGeoLocationCallback(new GeoLocationProvider.GeoLocationCallback() {
            @Override
            public void locationUpdated(Location location) {
                SharedManager.init(context);
                SharedManager.update(location);
            }
        });
        geoLocationProvider.tryRetrieveLocation();
    }
}
