package com.huseyinbulbul.gpsfused;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SharedManager {
    private static SharedPreferences preferences;
    public static void init(Context context){
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void update(Location latLng){
        String litSrc = preferences.getString("info", "[]");
        Gson gson = new Gson();
        List<Info> infos = gson.fromJson(litSrc, new TypeToken<List<Info>>(){}.getType());
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy - HH:mm");

        String location = latLng.getLatitude()+","+latLng.getLongitude();
        infos.add(new Info(format.format(new Date(System.currentTimeMillis())), location));

        SharedPreferences.Editor editor = preferences.edit();
        litSrc = gson.toJson(infos);
        editor.putString("info", litSrc);
        editor.commit();
    }

    public static void updateError(Exception e){
        String litSrc = preferences.getString("errors", "[]");
        Gson gson = new Gson();
        List<Error> errors = gson.fromJson(litSrc, new TypeToken<List<Error>>(){}.getType());
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy - HH:mm");

        errors.add(new Error(format.format(new Date(System.currentTimeMillis())), e.getMessage()));

        SharedPreferences.Editor editor = preferences.edit();
        litSrc = gson.toJson(errors);
        editor.putString("errors", litSrc);
        editor.commit();
    }

    public static void updateError(String e){
        String litSrc = preferences.getString("errors", "[]");
        Gson gson = new Gson();
        List<Error> errors = gson.fromJson(litSrc, new TypeToken<List<Error>>(){}.getType());
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy - HH:mm");

        errors.add(new Error(format.format(new Date(System.currentTimeMillis())), e));

        SharedPreferences.Editor editor = preferences.edit();
        litSrc = gson.toJson(errors);
        editor.putString("errors", litSrc);
        editor.commit();
    }

    public static String getText(){
        return preferences.getString("info", "[]");
    }

    public static String getErrorText(){
        return preferences.getString("errors", "[]");
    }

    public static String getGeofencesText(){
        return preferences.getString("geofences", "[]");
    }

    public static void updateGeofenceEvent(String str){
        String litSrc = preferences.getString("geofences", "[]");
        Gson gson = new Gson();
        List<Info> infos = gson.fromJson(litSrc, new TypeToken<List<Info>>(){}.getType());
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy - HH:mm");

        infos.add(new Info(format.format(new Date(System.currentTimeMillis())), str));

        SharedPreferences.Editor editor = preferences.edit();
        litSrc = gson.toJson(infos);
        editor.putString("geofences", litSrc);
        editor.commit();
    }
}
