package com.huseyinbulbul.gpsfused;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;

public class MainActivity extends AppCompatActivity {
//41.117328, 29.023978 maslak metro
//41.021681, 29.016526 ahmediye
//41.016664, 29.023760 carrefour
//41.015539, 29.028921 burhan felek
//41.012989, 29.021250 kapıağası
//41.019918, 29.034361 academic
//41.021213, 29.038331 capitol
//41.006379, 29.024846 isfalt
//41.000153, 29.030371 natulius
    private int page = 0;
    private Button pageButton;
    private Button btService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        AlarmManager am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
//        Intent intent = new Intent(this, AlarmReciever.class);
//        PendingIntent pi = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
//
//        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 60000L, 60000L, pi);




        final TextView textView = findViewById(R.id.tv_hello);
        pageButton = findViewById(R.id.bt_error);
        btService = findViewById(R.id.bt_location);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AlarmReciever.class);
                PendingIntent sender = PendingIntent.getBroadcast(MainActivity.this, 0, intent, 0);
                AlarmManager alarmManager = (AlarmManager) MainActivity.this.getSystemService(Context.ALARM_SERVICE);
                alarmManager.cancel(sender);
            }
        });

        SharedManager.init(this);
        textView.setText(SharedManager.getText());

        (findViewById(R.id.bt_geo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createGeofences();
            }
        });

        pageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (page % 3){
                    case 0:
                        textView.setText(SharedManager.getText());
                        pageButton.setText("next page errors");
                        break;

                    case 1:
                        textView.setText(SharedManager.getErrorText());
                        pageButton.setText("next page geofences");
                        break;

                    case 2:
                        textView.setText(SharedManager.getGeofencesText());
                        pageButton.setText("next page alarms");
                        break;
                }
                page++;
            }
        });

        if(LocationService.isIsServiceRunning()){
            btService.setText("stop service");
        }else {
            btService.setText("start service");
        }

        btService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(LocationService.isIsServiceRunning()){
                    stopService(new Intent(MainActivity.this, LocationService.class));
                }else {
                    startService(new Intent(MainActivity.this, LocationService.class));
                }
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void createGeofences(){
        createGeofence(41.117328, 29.023978, "maslak", 13590);
        createGeofence(41.021681, 29.016526 , "ahmediye", 135791);
        createGeofence(41.016664, 29.023760, "carrefour", 135792);
        createGeofence(41.015539, 29.028921, "burhan", 135793);
        createGeofence(41.012989, 29.021250, "kapıağası", 135794);
        createGeofence(41.019918, 29.034361, "academic", 135795);
        createGeofence(41.021213, 29.038331, "capitol", 135796);
        createGeofence(41.006379, 29.024846, "isfalt", 135797);
        createGeofence(41.000153, 29.030371, "natulius", 135798);
    }

    @SuppressLint("MissingPermission")
    private void createGeofence(double lat, double lon, final String id, int idd){
        GeofencingClient client = LocationServices.getGeofencingClient(this);

        Geofence.Builder builder = new Geofence.Builder()
                .setCircularRegion(lat , lon, 100)
                .setRequestId(id)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_EXIT|Geofence.GEOFENCE_TRANSITION_ENTER)
                .setExpirationDuration(1000 * 60 * 60 * 24);//3 hours

        GeofencingRequest.Builder request = new GeofencingRequest.Builder();
        request.addGeofence(builder.build());

        client.addGeofences(request.build(), getGeofencePendingIntent(idd))
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        SharedManager.updateError(id+" added");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        SharedManager.updateError(id+" cannot be added");
                    }
                });
    }

    private PendingIntent getGeofencePendingIntent(int id) {
        Intent intent = new Intent(this, GeofenceService.class);
        return PendingIntent.getService(this, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}