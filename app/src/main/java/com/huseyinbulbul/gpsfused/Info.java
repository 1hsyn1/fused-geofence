package com.huseyinbulbul.gpsfused;

public class Info {
    public String time;
    public String location;

    public Info(String time, String location) {
        this.time = time;
        this.location = location;
    }
}
